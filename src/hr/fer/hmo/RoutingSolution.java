package hr.fer.hmo;

import java.util.*;

/**
 * Created by dino on 04/01/15.
 */
public class RoutingSolution {

    private final List<Route> routes;

    private final RoutingProblem routingProblem;

    public RoutingSolution(RoutingProblem routingProblem) {
        this.routingProblem = routingProblem;
        routes = new ArrayList<>();
    }

    public RoutingSolution(RoutingProblem routingProblem, List<Route> routes) {
        this.routingProblem = routingProblem;
        this.routes = routes;
    }

    private void decimate() {
        int decimationCount = (int) Math.ceil(routes.size() / 8.0);
        for (int i = 0; i < decimationCount; i++) {
            int rand = RandomHelper.randomInt(routes.size() - 1);
            routes.remove(rand);
        }
    }

    private int getSupply(Warehouse warehouse) {
        int supply = warehouse.getCapacity();
        for (Route route : routes) {
            if (route.getWarehouse().equals(warehouse)) {
                supply -= route.getCargo();
            }
        }
        return supply;
    }

    private Set<Client> getRemainingClients() {
        Set<Client> visitedClients = new HashSet<>();
        for (Route route : routes) {
            visitedClients.addAll(route.getClients());
        }
        Set<Client> remainingClients = new HashSet<>(routingProblem.getClients());
        remainingClients.removeAll(visitedClients);
        return remainingClients;
    }

    private Edge findNearestWarehousePath(Client client) {
        int distance = Integer.MAX_VALUE;
        Edge nearestWarehousePath = null;
        for (Edge edge : client.getWarehousePaths()) {
            if (getSupply((Warehouse) edge.getOtherVertex(client)) >= routingProblem.getVehicleCapacity()
                    && edge.getWeight() < distance) {
                distance = edge.getWeight();
                nearestWarehousePath = edge;
            }
        }
        return nearestWarehousePath;
    }

    private Edge findNearestClientPath(Client client, Collection<Client> clients) {
        Edge nearestClientPath = null;
        int distance = Integer.MAX_VALUE;

        for (Edge edge : client.getClientPaths()) {
            if (clients.contains(edge.getOtherVertex(client)) && edge.getWeight() < distance) {
                distance = edge.getWeight();
                nearestClientPath = edge;
            }
        }
        return nearestClientPath;
    }

    public void solveGreedy() {
        Set<Client> remainingClients = getRemainingClients();

        while (!remainingClients.isEmpty()) {

            Client randomClient = RandomHelper.getRandomClient(new ArrayList<>(remainingClients));
            Edge nearestWarehousePath = findNearestWarehousePath(randomClient);
            Warehouse warehouse = (Warehouse) ((nearestWarehousePath.getFirst() instanceof Warehouse) ? nearestWarehousePath.getFirst() : nearestWarehousePath.getSecond());
            Route route = new Route(warehouse);
            route.addSegment(nearestWarehousePath);
            remainingClients.remove(randomClient);

            Client lastVisited = randomClient;
            Edge nearestClientPath;
            Client nearestClient;

            while (!remainingClients.isEmpty()) {
                nearestClientPath = findNearestClientPath(lastVisited, remainingClients);
                nearestClient = (Client) nearestClientPath.getOtherVertex(lastVisited);

                if(route.getCargo() + nearestClient.getDemand() <= routingProblem.getVehicleCapacity()) {
                    route.addSegment(nearestClientPath);
                    remainingClients.remove(nearestClient);
                    lastVisited = nearestClient;
                } else {
                    break;
                }
            }

            routes.add(route);
        }
    }

    public RoutingSolution nextSolution() {
        RoutingSolution routingSolution = new RoutingSolution(routingProblem, new ArrayList<>(routes));
        routingSolution.decimate();
        routingSolution.solveGreedy();
        return routingSolution;
    }

    public int getCost() {
        int cost = 0;
        Set<Warehouse> warehouses = new HashSet<>();
        for (Route route : routes) {
            warehouses.add(route.getWarehouse());
            cost += route.getCost();
        }
        for (Warehouse warehouse : warehouses) {
            cost += warehouse.getOpeningCost();
        }
        cost += routingProblem.getVehicleCost() * routes.size();
        return cost;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.valueOf(routes.size()));
        stringBuilder.append("\n\n");
        for (Route route : routes) {
            stringBuilder.append(route.toString());
            stringBuilder.append("\n\n");
        }
        stringBuilder.append("\n");
        stringBuilder.append(String.valueOf(getCost()));
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
