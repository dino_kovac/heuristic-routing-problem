package hr.fer.hmo;

/**
 * Created by dino on 03/01/15.
 */
public class Warehouse extends Vertex {

    private int capacity;

    private int openingCost;

    public Warehouse(String name, int xPosition, int yPosition) {
        super(name, xPosition, yPosition);
    }
    
    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getOpeningCost() {
        return openingCost;
    }

    public void setOpeningCost(int openingCost) {
        this.openingCost = openingCost;
    }

    @Override
    public String toString() {
        return "w" + super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Warehouse)) {
            return false;
        }
        Warehouse other = (Warehouse) obj;
        return name.equals(other.name);
    }
}
