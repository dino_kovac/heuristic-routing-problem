package hr.fer.hmo;

import java.util.List;

/**
 * Created by dino on 04/01/15.
 */
public class RandomHelper {

    public static double random(double x1, double x12) {
        return x1 + (int) (Math.random() * ((x12 - x1) + 1));
    }

    public static double random(float max) {
        return random(0, max);
    }

    public static double random() {
        return random(0, 1);
    }

    public static int randomInt(int min, int max) {
        return (int) Math.round(random(min, max));
    }

    public static int randomInt(int max) {
        return randomInt(0, max);
    }

    public static Client getRandomClient(List<Client> clients) {
        int rand = RandomHelper.randomInt(clients.size() - 1);
        return clients.get(rand);
    }

}
