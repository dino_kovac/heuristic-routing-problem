package hr.fer.hmo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dino on 04/01/15.
 */
public class Route {

    private final Warehouse warehouse;

    private final List<Edge> segments;

    private Client lastClient;

    private List<Client> clients;

    private int cost;

    private int cargo;

    public Route(Warehouse warehouse) {
        this.warehouse = warehouse;
        segments = new ArrayList<>();
        clients = new ArrayList<>();
        cost = 0;
        cargo = 0;
    }

    public void addSegment(Edge segment) {
        Vertex lastVisited = (lastClient == null) ? warehouse : lastClient;

        if (segment.getFirst().equals(lastVisited)) {
            addEdge(segment, (Client) segment.getSecond());
        } else if (segment.getSecond().equals(lastVisited)) {
            addEdge(segment, (Client) segment.getFirst());
        }
    }

    private void addEdge(Edge edge, Client lastClient) {
        this.lastClient = lastClient;
        segments.add(edge);
        clients.add(lastClient);
        cost += edge.getWeight();
        cargo += lastClient.getDemand();
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public int getCost() {
        Edge returnSegment = null;
        for (Edge edge : lastClient.getEdges()) {
            if((edge.getFirst().equals(warehouse) && edge.getSecond().equals(lastClient))
                    || (edge.getFirst().equals(lastClient) && edge.getSecond().equals(warehouse))) {
                returnSegment = edge;
                break;
            }
        }
        return cost + returnSegment.getWeight();
    }

    public int getCargo() {
        return cargo;
    }

    public List<Client> getClients() {
        return clients;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(warehouse.getName());
        stringBuilder.append(": ");
        Vertex lastVisited = warehouse;
        for (Edge segment : segments) {

            if(segment.getFirst().equals(lastVisited)) {
                lastVisited = segment.getSecond();
            } else {
                lastVisited = segment.getFirst();
            }

            stringBuilder.append(" ");
            stringBuilder.append(lastVisited.getName());
        }
        return stringBuilder.toString();
    }
}
