package hr.fer.hmo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Need filename for argument! Aborting.");
            System.exit(-1);
        }

        String filename = args[0];

        RoutingProblem problem = new RoutingProblem();
        List<Warehouse> warehouses = new ArrayList<>();
        List<Client> clients = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(filename))) {

            int clientCount = scanner.nextInt();
            int warehouseCount = scanner.nextInt();

            for (int i = 0; i < warehouseCount; ++i) {
                int xPos = scanner.nextInt();
                int yPos = scanner.nextInt();
                warehouses.add(new Warehouse(String.valueOf(i), xPos, yPos));
            }

            for (int i = 0; i < clientCount; ++i) {
                int xPos = scanner.nextInt();
                int yPos = scanner.nextInt();
                clients.add(new Client(String.valueOf(i), xPos, yPos));
            }

            problem.setVehicleCapacity(scanner.nextInt());

            for (int i = 0; i < warehouseCount; ++i) {
                warehouses.get(i).setCapacity(scanner.nextInt());
            }

            for (int i = 0; i < clientCount; ++i) {
                clients.get(i).setDemand(scanner.nextInt());
            }

            for (int i = 0; i < warehouseCount; ++i) {
                warehouses.get(i).setOpeningCost(scanner.nextInt());
            }

            problem.setVehicleCost(scanner.nextInt());

            Edge edge;

            List<Vertex> vertices = new ArrayList<>(warehouses.size() + clients.size());
            vertices.addAll(warehouses);
            vertices.addAll(clients);
            for (int i = 0; i < vertices.size(); i++) {
                for (int j = i + 1; j < vertices.size(); j++) {
                    if(!(vertices.get(i) instanceof Warehouse && vertices.get(j) instanceof Warehouse)) {
                        edge = new Edge(Edge.WEIGHT_UNINITIALIZED, vertices.get(i), vertices.get(j));
                        problem.addEdge(edge);
                    }
                }
            }

            SimulatedAnnealing simulatedAnnealing = new SimulatedAnnealing(problem, 1000);
            simulatedAnnealing.run();
            System.out.println(simulatedAnnealing.getCurrentSolution().toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static RoutingSolution sampleSolution(RoutingProblem routingProblem) {
        List<Route> routes = new ArrayList<>();
        routes.add(createRoute(routingProblem, 1, new int[] {85, 96, 31, 14}));
        routes.add(createRoute(routingProblem, 4, new int[] {24, 86, 87, 74}));
        routes.add(createRoute(routingProblem, 3, new int[] {28, 47, 78, 76, 6}));
        routes.add(createRoute(routingProblem, 1, new int[] {16, 9, 64, 81}));
        routes.add(createRoute(routingProblem, 3, new int[] {2, 29, 33, 88, 38}));
        routes.add(createRoute(routingProblem, 3, new int[] {65, 89, 91, 8, 54}));
        routes.add(createRoute(routingProblem, 3, new int[] {55, 62, 17, 77}));
        routes.add(createRoute(routingProblem, 1, new int[] {46, 40, 90, 20}));
        routes.add(createRoute(routingProblem, 1, new int[] {84, 50, 1, 27}));
        routes.add(createRoute(routingProblem, 1, new int[] {13, 26, 94, 97}));
        routes.add(createRoute(routingProblem, 1, new int[] {43, 75, 0, 32}));
        routes.add(createRoute(routingProblem, 1, new int[] {30, 39, 83}));
        routes.add(createRoute(routingProblem, 3, new int[] {45, 35, 25, 15}));
        routes.add(createRoute(routingProblem, 2, new int[] {80, 68, 36, 4}));
        routes.add(createRoute(routingProblem, 4, new int[] {12, 95, 59, 66}));
        routes.add(createRoute(routingProblem, 4, new int[] {73, 19, 48, 72}));
        routes.add(createRoute(routingProblem, 3, new int[] {53, 57, 44, 99, 70}));
        routes.add(createRoute(routingProblem, 3, new int[] {92, 60, 93, 63}));
        routes.add(createRoute(routingProblem, 1, new int[] {7, 11, 21, 56, 51}));
        routes.add(createRoute(routingProblem, 3, new int[] {18, 49, 23, 42}));
        routes.add(createRoute(routingProblem, 4, new int[] {69, 10, 82, 22}));
        routes.add(createRoute(routingProblem, 3, new int[] {41, 3, 71, 61, 5}));
        routes.add(createRoute(routingProblem, 1, new int[] {52, 98, 58, 37}));
        routes.add(createRoute(routingProblem, 3, new int[] {67, 34, 79}));
        return new RoutingSolution(routingProblem, routes);
    }

    public static Route createRoute(RoutingProblem routingProblem, int warehouse, int[] clients) {
        Route r = new Route((Warehouse) routingProblem.getWarehouses().toArray()[warehouse]);
        r.addSegment(routingProblem.getEdge("w" + warehouse, "c" + clients[0]));
        for (int i = 0; i < clients.length - 1; i++) {
            r.addSegment(routingProblem.getEdge("c" + clients[i], "c" + clients[i + 1]));
        }
        return r;
    }
}
