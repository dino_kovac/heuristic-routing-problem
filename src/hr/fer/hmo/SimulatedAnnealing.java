package hr.fer.hmo;

/**
 * Created by dino on 04/01/15.
 */
public class SimulatedAnnealing implements Runnable {

    private final RoutingProblem routingProblem;

    private double temperature;

    private RoutingSolution currentSolution;

    public SimulatedAnnealing(RoutingProblem routingProblem, double temperature) {
        this.routingProblem = routingProblem;
        this.temperature = temperature;

        currentSolution = new RoutingSolution(routingProblem);
        currentSolution.solveGreedy();
    }

    private RoutingSolution getNeighbouringSolution() {
        return currentSolution.nextSolution();
    }

    private void decreaseTemperature() {
        temperature = temperature * 0.9999;
    }

    private double calculateAcceptanceProbability(RoutingSolution solution) {
        int currentCost = currentSolution.getCost();
        int cost = solution.getCost();

        if(cost < currentCost) {
            return 1;
        } else {
            int difference = Math.abs(currentCost - cost);

            return Math.exp(-difference / temperature);
        }
    }

    public RoutingSolution getCurrentSolution() {
        return currentSolution;
    }

    @Override
    public void run() {
        while(temperature > 0.01) {
            RoutingSolution neighbouringSolution = getNeighbouringSolution();
            double probability = calculateAcceptanceProbability(neighbouringSolution);
            if(RandomHelper.random() < probability) {
                currentSolution = neighbouringSolution;
            }
            decreaseTemperature();
        }
    }
}
