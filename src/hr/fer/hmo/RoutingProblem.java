package hr.fer.hmo;

import java.util.LinkedHashSet;
import java.util.Set;

public class RoutingProblem {

    private Set<Warehouse> warehouses;

    private Set<Client> clients;

    private Set<Edge> edges;

    private int vehicleCapacity;

    private int vehicleCost;

    public RoutingProblem() {
        warehouses = new LinkedHashSet<>();
        clients = new LinkedHashSet<>();
        edges = new LinkedHashSet<>();
    }

    public void addVertex(Vertex v) {
        addVertex(v, true);
    }

    public void addVertex(Vertex v, boolean addEdges) {
        if (v instanceof Warehouse) {
            warehouses.add((Warehouse) v);
        } else if (v instanceof Client) {
            clients.add((Client) v);
        }
        if (addEdges) {
            edges.addAll(v.getEdges());
        }
    }

    public void addEdge(Edge e) {
        edges.add(e);
        addVertex(e.getFirst(), false);
        addVertex(e.getSecond(), false);
    }

    public Edge getEdge(String first, String second) {
        for (Edge edge : edges) {
            if (edge.getFirst().toString().equals(first) && edge.getSecond().toString().equals(second)) {
                return edge;
            } else if (edge.getFirst().toString().equals(second) && edge.getSecond().toString().equals(first)) {
                return edge;
            }
        }
        return null;
    }

    public Set<Warehouse> getWarehouses() {
        return warehouses;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public int getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(int vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public int getVehicleCost() {
        return vehicleCost;
    }

    public void setVehicleCost(int vehicleCost) {
        this.vehicleCost = vehicleCost;
    }
}