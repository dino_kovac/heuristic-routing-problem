package hr.fer.hmo;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by dino on 12/01/14.
 */
public abstract class Vertex implements Comparable<Vertex> {

    protected String name;

    protected Set<Vertex> neighbours;

    protected Set<Edge> edges;

    protected Set<Edge> warehousePaths;

    protected Set<Edge> clientPaths;

    protected int positionX;

    protected int positionY;

    public Vertex(String name, int positionX, int positionY) {
        this.name = name;
        this.positionX = positionX;
        this.positionY = positionY;
        neighbours = new LinkedHashSet<>();
        edges = new LinkedHashSet<>();
        warehousePaths = new LinkedHashSet<>();
        clientPaths = new LinkedHashSet<>();
    }

    public void addEdge(Edge e) {
        edges.add(e);

        if (this.equals(e.getFirst())) {
            addNeighbour(e.getSecond());
            addPath(e, e.getSecond());
        } else if (this.equals(e.getSecond())) {
            addNeighbour(e.getFirst());
            addPath(e, e.getFirst());
        }
    }

    protected void addNeighbour(Vertex v) {
        neighbours.add(v);
    }

    public void addNeighbour(Vertex v, int weight) {
        addNeighbour(v);
        edges.add(new Edge(weight, this, v));
    }

    private void addPath(Edge edge, Vertex neighbour) {
        if (neighbour instanceof Warehouse) {
            warehousePaths.add(edge);
        } else {
            clientPaths.add(edge);
        }
    }

    public Set<Edge> getWarehousePaths() {
        return warehousePaths;
    }

    public Set<Edge> getClientPaths() {
        return clientPaths;
    }

    public String getName() {
        return name;
    }

    public Set<Vertex> getNeighbours() {
        return neighbours;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public int compareTo(Vertex o) {
        return name.compareTo(o.name);
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }
}
