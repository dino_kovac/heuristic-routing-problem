package hr.fer.hmo;

/**
 * Created by dino on 03/01/15.
 */
public class Client extends Vertex {

    private int demand;

    public Client(String name, int xPosition, int yPosition) {
        super(name, xPosition, yPosition);
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    @Override
    public String toString() {
        return "c" + super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Client)) {
            return false;
        }
        Client other = (Client) obj;
        return name.equals(other.name);
    }
}
